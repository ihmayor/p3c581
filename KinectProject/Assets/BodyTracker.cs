﻿using NetworkIt;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Windows.Kinect;

public class BodyTracker : MonoBehaviour
{

    //use this variable in the inspector to finetune your gesture detection.
    public float sittingThreshold = -5.0f;
    //use this variable in the inspector to finetune your gesture detection.
    public float angleTolerance = 10;
    public Dictionary<string, GameObject> birds;
    private List<Vector2> birdPos;
    private float minY = -219;
    private float minX = 61;
    private float maxY = -53;
    private float maxX = 127;
    private int birdIndex = 0;

    private List<BodyGameObject> bodies = new List<BodyGameObject>();

    public NetworkItClient networkInterface;
    private float seconds = 4f;
    private bool startWave = false;
    private float currentTime;
    private Vector3 waveStartPos;
    private AudioClip[] birdSounds;


    public int leaf_num = 0;
    public int bird_num = 0;



    //remember to use late update for after the KinectManager has updated all sensor information
    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            SendEnter();
        }

        //if (Input.GetKeyDown(KeyCode.K))
        //{
        //    SendHappy();
        //}

        //if (Input.GetKeyDown(KeyCode.L))
        //{
        //    SendSad();
        //}
        //if (Input.GetKeyDown(KeyCode.Y))
        //    SendLeave();    
        if (bodies.Count > 0)
        {
            //some bodies, send orientation update
            Vector3 shoulderLeft = bodies[0].GetJoint(Windows.Kinect.JointType.ShoulderLeft).transform.localPosition;
            Vector3 elbowLeft = bodies[0].GetJoint(Windows.Kinect.JointType.ElbowLeft).transform.localPosition;
            Vector3 wristLeft = bodies[0].GetJoint(Windows.Kinect.JointType.WristLeft).transform.localPosition;

            Vector3 elbowShoulder = shoulderLeft - elbowLeft;
            Vector3 elbowWrist = wristLeft - elbowLeft;


            //some bodies, send orientation update
            Vector3 shoulderRight = bodies[0].GetJoint(Windows.Kinect.JointType.ShoulderRight).transform.localPosition;
            Vector3 elbowRight = bodies[0].GetJoint(Windows.Kinect.JointType.ElbowRight).transform.localPosition;
            Vector3 wristRight = bodies[0].GetJoint(Windows.Kinect.JointType.WristRight).transform.localPosition;

            Vector3 elbowShoulderRight = shoulderRight - elbowRight;
            Vector3 elbowWristRight = wristRight - elbowRight;

            Vector3 HandRight = bodies[0].GetJoint(JointType.HandRight).transform.localPosition;


            Vector3 HandLeft = bodies[0].GetJoint(JointType.HandLeft).transform.localPosition;
            HandState lhs = bodies[0].LeftHandState;
            HandState rhs = bodies[0].RighttHandState;
            elbowShoulder.Normalize();
            elbowWrist.Normalize();
            elbowShoulderRight.Normalize();
            elbowWristRight.Normalize();

            float armBendAngle = Vector3.SignedAngle(elbowShoulder, elbowWrist, Vector3.right);
            float armBendAngleRight = Vector3.SignedAngle(elbowShoulderRight, elbowWristRight, Vector3.left);

            if (InRange(armBendAngle, 180, angleTolerance) || InRange(armBendAngle, -180, angleTolerance) || InRange(armBendAngleRight, 180, angleTolerance) || InRange(armBendAngleRight, -180, angleTolerance))
            {
                if (Time.time - currentTime >= seconds)
                {
                    StartCoroutine(AllLeavesFall());
                    currentTime = Time.time;
                }

            }
            else if (InRange(armBendAngle, 90, angleTolerance) || InRange(armBendAngleRight, 90, angleTolerance))
            {
                if (Time.time - currentTime >= seconds)
                {
                    currentTime = Time.time;
                    if (birds.Count > 0)
                    {
                        SendHappy();
                        bird_num++;
                    }
                    else
                    {
                        leaf_num++;
                        GameObject leaf = Resources.Load<GameObject>("LeafAnimGreen");
                        GameObject instance = Instantiate(leaf);
                        instance.SendMessage("SetWave");
                        instance.transform.parent = GameObject.Find("Canvas").transform;
                        instance.transform.localPosition = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY));
                        Debug.Log(instance.transform.localPosition);
                    }
                }

            }
            else if (InRange(armBendAngle, -90, angleTolerance) || InRange(armBendAngleRight, -90, angleTolerance))
            {
                if (Time.time - currentTime >= seconds)
                {
                    currentTime = Time.time;
                    if (birds.Count > 0)
                    {
                        SendSad();
                        bird_num--;
                    }
                    else
                    {
                        GameObject leaf = Resources.Load<GameObject>("LeafAnimRed");
                        GameObject instance = Instantiate(leaf);
                        instance.SendMessage("SetWave");
                        instance.transform.parent = GameObject.Find("Canvas").transform;
                        instance.transform.localPosition = new Vector3(Random.Range(minX, maxX), Random.Range(minY, maxY));
                        Debug.Log(instance.transform.localPosition);
                    }
                }
            }
        }
        else
        {
            SendLeave();
        }
    }


    private IEnumerator AllLeavesFall()
    {
        GameObject[] gs = GameObject.FindGameObjectsWithTag("Leaf");
        foreach (GameObject g in gs)
        {
            if (!g.name.Contains("Anim"))
            {
                Rigidbody r;
                if (g.GetComponent<Rigidbody>() == null)
                    r = g.AddComponent<Rigidbody>();
                else
                    r = g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.velocity *= 1.001f;
            }
        }
        yield return new WaitForSeconds(5);
        foreach (GameObject g in gs)
        {
            Destroy(g);
        }
    }

    private bool InRange(float value, float targetValue, float delta)
    {
        return (value >= (targetValue - delta)) && (value <= (targetValue + delta));
    }

    private void Start()
    {
        birds = new Dictionary<string, GameObject>();
        birdPos = new List<Vector2>();
        birdPos.Add(new Vector2(282, -134));
        birdPos.Add(new Vector2(282, -51f));
        birdPos.Add(new Vector2(329, -70f));
        birdPos.Add(new Vector2(329, -166));
        birdPos.Add(new Vector2(116f, -190f));

        AudioClip a = Resources.Load<AudioClip>("bird1");
        AudioClip a1 = Resources.Load<AudioClip>("bird2");
        AudioClip a2 = Resources.Load<AudioClip>("bird3");
        AudioClip a3 = Resources.Load<AudioClip>("bird4");
        birdSounds = new AudioClip[] { a, a1, a2, a3 };
    }

    public void SendEnter()
    {
        Message m = new Message("Enter");
        networkInterface.SendMessage(m);
        GameObject[] leaves = GameObject.FindGameObjectsWithTag("Leaf");
        foreach (GameObject leaf in leaves)
        {
            if (leaf.name.Contains("Anim"))
                leaf.SendMessage("SetWave");
        }

    }
    public void SendHappy()
    {
        Debug.Log("send happy");
        Message m = new Message("Happy");
        networkInterface.SendMessage(m);
    }

    public void SendSad()
    {
        Debug.Log("send sad");
        Message m = new Message("Sad");
        networkInterface.SendMessage(m);
    }

    public void NetworkIt_Message(object m)
    {
        Message message = (Message)m;
        Debug.Log("Message: " + message.Subject);
        if (message.Subject.Contains("AddBird"))
        {
            string id = message.GetField("ID");
            if (birds.ContainsKey(id))
                return;
            GameObject bird = Resources.Load<GameObject>("BirdAnimator");
            GameObject bInstance = Instantiate(bird);
            bInstance.transform.parent = GameObject.Find("Canvas").transform;
            birds.Add(id, bInstance);
            if (birdIndex < birdPos.Count)
                bInstance.transform.localPosition = birdPos[birdIndex];
            else
                bInstance.transform.localPosition = birdPos[birdIndex % birdPos.Count] + new Vector2(birdIndex * 10, birdIndex * 5);
            Debug.Log(bInstance.transform.position);
            birdIndex++;
        }
        else if (message.Subject.Contains("RemoveBird"))
        {
            string id = message.GetField("ID");
            Destroy(birds[id]);
            birds.Remove(id);
        }
        else if (message.Subject.Contains("Chirp"))
        {
            string id = message.GetField("ID");
            birds[id].GetComponent<BirdScript>().Hop();
            UnityEngine.AudioSource aud = GetComponent<UnityEngine.AudioSource>();
            int index = Mathf.FloorToInt(Random.Range(0, 3));
            aud.PlayOneShot(birdSounds[index]);
        }

    }

    void Kinect_BodyFound(object args)
    {
        BodyGameObject bodyFound = (BodyGameObject)args;
        bodies.Add(bodyFound);

        AudioClip clip = Resources.Load<AudioClip>("looperman-l-1850970-0102276-zhonzell-momma-dont-cry");
        UnityEngine.AudioSource aud = gameObject.GetComponent<UnityEngine.AudioSource>();
        if (!aud.isPlaying)
            aud.PlayOneShot(clip);
        SendEnter();
    }

    void Kinect_WaveRecieved(object args)
    {
        //ulong bodyId = (ulong)args;
        //Debug.Log("wAVE received");
        //GameObject gobj = GameObject.Find("Canvas").transform.Find("Panel").gameObject;
        //if (gobj.GetComponent<Image>().color == Color.yellow)
        //    gobj.GetComponent<Image>().color = Color.magenta;
        //else
        //    gobj.GetComponent<Image>().color = Color.yellow;
    }

    public void NetworkIt_Connect(object args)
    {
        Message m = new Message("Check");
        networkInterface.SendMessage(m);
    }

    void Kinect_BodyLost(object args)
    {
        GameObject gobj = GameObject.Find("Canvas").transform.Find("Panel").gameObject;
        gobj.GetComponent<Image>().color = Color.white;

        ulong bodyDeletedId = (ulong)args;
        if (bodies.Count <= 1)
        {
            GameObject[] leaves = GameObject.FindGameObjectsWithTag("Leaf");
            foreach (GameObject leaf in leaves)
            {
                if (leaf.name.Contains("Anim"))
                    leaf.SendMessage("UnSetWave");
            }
            SendLeave();
        }

        lock (bodies)
        {
            foreach (BodyGameObject bg in bodies)
            {
                if (bg.ID == bodyDeletedId)
                {
                    bodies.Remove(bg);
                    return;
                }
            }


        }
    }
    private void SendLeave()
    {
        Message m = new Message("Leave");
        networkInterface.SendMessage(m);

    }

}