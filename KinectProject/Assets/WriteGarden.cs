﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class WriteGarden : MonoBehaviour {
    public GameObject flower;
    public GameObject bird;
    private static string read;
    private static string read2;
    private float time;
    static void WriteString(string newVal,string newVal2)
    {
        string path = "Assets/Resources/garden.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, false);
        writer.WriteLine(newVal);
        writer.WriteLine(newVal2);
        writer.Close();

    }

    static void ReadString()
    {
        string path = "Assets/Resources/garden.txt";

        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(path);

        read = reader.ReadLine();
        read2 = reader.ReadLine();
        reader.Close();
    }


    // Use this for initialization
    void Start () {
        time = Time.time;
        int i;
        int i2;
        ReadString();
        int.TryParse(read,out i);
        GetComponent<BodyTracker>().leaf_num = i;
        int.TryParse(read2, out i2);
        GetComponent<BodyTracker>().bird_num = i2;
    }


    // Update is called once per frame
    void Update () {
        int leafN = GetComponent<BodyTracker>().leaf_num;
        int birdN = GetComponent<BodyTracker>().bird_num;
        if (leafN > 20 && !flower.activeSelf)
        {
            flower.SetActive(true);
            WriteString(leafN.ToString(), birdN.ToString());
        }
        if (birdN > 20 && !bird.activeSelf)
        {
            bird.SetActive(true);
            WriteString(leafN.ToString(), birdN.ToString());
        }
        if (Time.time - time > 30)
        {
            WriteString(leafN.ToString(), birdN.ToString());
            time = Time.time;
        }
    }
}
