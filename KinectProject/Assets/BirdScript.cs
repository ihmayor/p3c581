﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
    }

    public void Hop()
    {
        GetComponent<Animator>().ResetTrigger("Hop");
        GetComponent<Animator>().SetTrigger("Hop");
    }
}
