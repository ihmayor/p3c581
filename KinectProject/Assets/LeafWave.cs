﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeafWave : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void SetWave()
    {
        GetComponent<Animator>().ResetTrigger("Wind");
        GetComponent<Animator>().ResetTrigger("NoWind");
        GetComponent<Animator>().SetTrigger("Wind");
    }

    void UnSetWave()
    {
        GetComponent<Animator>().ResetTrigger("Wind");
        GetComponent<Animator>().ResetTrigger("NoWind");
        GetComponent<Animator>().SetTrigger("NoWind");
    }


}
