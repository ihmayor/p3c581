﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageBouncer : MonoBehaviour {

    float t;
	// Use this for initialization
	void Start () {
        GetComponent<Rigidbody>().AddForce(RandomDirection()*6000);
        t = Time.time;
    }
	
	// Update is called once per frame
	void Update () {
		if (Time.time - t >= 5)
        {
            GetComponent<Rigidbody>().AddForce(RandomDirection() * 6000);
            transform.Rotate(Vector3.up, Time.deltaTime * 25f);
            t = Time.time;
        }
        if (transform.position.x > 900 || transform.position.y > 900 || transform.position.x < -600 || transform.position.y < -900)
        {
            transform.position = new Vector2(100, 100);
        }
        
    }

    public void CheckPos(Vector2 position)
    {
        float r = 100;
        if (Vector3.Distance(position, transform.position) <= r)
        {
            OnPress();
        }

    }

    Vector3 RandomDirection()
    {

        var check = Mathf.FloorToInt(Random.Range(0, 4));
        switch (check)
        {
            case 0:
                return Vector3.up;
            case 1:
                return Vector3.down;
            case 2:
                return Vector3.right;
            case 3:
                return Vector3.left;
            default:
                return Vector3.left;
       }
    }

    public void OnPress()
    {
        GameObject.Find("ObjInfo").GetComponent<ObjectPanel>().OpenClosePanel(this.gameObject.name);
    }
}
