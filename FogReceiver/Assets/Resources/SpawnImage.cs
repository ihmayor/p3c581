﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnImage : MonoBehaviour {
    GameObject muffin;
    GameObject bird;
    GameObject ski;
    GameObject cheer;
    GameObject cake;
    GameObject finger;
    List<GameObject> allHappy;
    List<GameObject> allSad;
    // Use this for initialization
    void Start () {
        muffin = Resources.Load<GameObject>("photostocrop/Muffin");
        bird = Resources.Load<GameObject>("photostocrop/Bird");
        ski = Resources.Load<GameObject>("photostocrop/Ski");
        cheer = Resources.Load<GameObject>("photostocrop/Cheer");
        cake = Resources.Load<GameObject>("photostocrop/Cake");
        finger = Resources.Load<GameObject>("photostocrop/FingerPuppet");
        allHappy = new List<GameObject>() { bird,cheer, muffin };
        allSad = new List<GameObject>() { cake, ski, finger };
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void CreateHappy()
    {
        Debug.Log("Created Happy");
        int index = Mathf.FloorToInt(UnityEngine.Random.Range(0, allHappy.Count-1));
        GameObject p = GameObject.Find("Canvas").transform.Find("Icon").gameObject;

        GameObject g = Instantiate(allHappy[index]);
        g.transform.SetParent(p.transform);
        g.transform.localPosition = new Vector2(100, 100);
    }

    public void CreateCheerup()
    {
        Debug.Log("Created Sad");
        int index = Mathf.FloorToInt(UnityEngine.Random.Range(0, allSad.Count-1));
        GameObject p = GameObject.Find("Canvas").transform.Find("Icon").gameObject;

        GameObject g = Instantiate(allHappy[index]);
        g.transform.SetParent(p.transform);
    }
}
