﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IsConnected : MonoBehaviour {

	// Use this for initialization
	void Start () {
       		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void NetworkIt_Connect(object args)
    {
        EventArgs eventArgs = (EventArgs)args;
        GetComponent<Image>().color = new Color(0.0f, 1.0f, 0.0f);
    }

    public void NetworkIt_Disconnect(object args)
    {
        EventArgs eventArgs = (EventArgs)args;
        GetComponent<Image>().color = new Color(1.0f, 0.0f, 0.0f);
    }
}
