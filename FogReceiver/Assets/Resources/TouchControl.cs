﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchControl : MonoBehaviour
{
    public GameObject fog;
    public GameObject window;
    public GameObject feedback;
    public bool enter;
    private float time;

    // Use this for initialization
    void Start()
    {
        enter = false;
        time = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (!enter)
        {
            fog.SetActive(false);
            return;
        }
       else if (Input.touches.Length <= 0 && enter)
        {
            fog.SetActive(true);
            StartCoroutine(FadeObjectIn(fog, 0, 0.2f));
            return;
        }
        else if (!fog.activeSelf) return;
        switch (Input.GetTouch(0).phase)
        {
            case TouchPhase.Began:
                StartCoroutine(FadeInFeedback());
                GetComponent<SendPokeTemplate>().SendChirp();
                break;
            case TouchPhase.Ended:
                StopAllCoroutines();
                fog.SetActive(true);
                StartCoroutine(FadeOutTouch());
                break;
            case TouchPhase.Moved:
                StartCoroutine(FadeInWindow());
                break;
            case TouchPhase.Stationary:
                StartCoroutine(FadeInWindow());
                break;
            default:
                StopAllCoroutines();
                StartCoroutine(FadeOutTouch());
                break;
        }

        float offsetX = -197;
        float offsetY = -90;
        GameObject feedbackObj = feedback;
        GameObject fogObj = window;
        fog.transform.position = Input.GetTouch(0).position + new Vector2(offsetX, offsetY);
        feedbackObj.transform.position = Input.GetTouch(0).position + new Vector2(offsetX, offsetY);
        fogObj.transform.position = Input.GetTouch(0).position + new Vector2(offsetX, offsetY);
        if (Time.time - time > 0.3f &&fog.GetComponent<Image>().color.a < 0.7f)
        {
            time = Time.time;
            foreach(GameObject g in GameObject.FindGameObjectsWithTag("Thought"))
            {
                Debug.Log("check Position");
                g.SendMessage("CheckPos", Input.GetTouch(0).position);
            }
        }

    }



    private IEnumerator FadeInFeedback()
    {
        StartCoroutine(FadeObjectIn(feedback, 0, 0.2f));
        StartCoroutine(FadeObjectOut(fog, 0, 0.3f));
        yield return new WaitUntil(() => fog.GetComponent<Image>().color.a == 0);
        fog.SetActive(false);
        yield return null;
    }

    private IEnumerator FadeInWindow()
    {
        StartCoroutine(FadeObjectIn(window, 0, 0.2f));
        GameObject feedbackObj = feedback;
        StartCoroutine(FadeObjectOut(feedbackObj, 0, 0.3f));
        yield return null;
           
    }


    private IEnumerator FadeOutTouch()
    {
        GameObject fogObj = window;
        StartCoroutine(FadeObjectOut(fogObj, 0, 0.1f));
        GameObject feedbackObj = feedback;
        StartCoroutine(FadeObjectOut(feedbackObj, 0, 0.1f));
        fog.SetActive(true);
        StartCoroutine(FadeObjectIn(fog, 0, 0.2f));
        yield return null;
    }


    IEnumerator FadeObjectIn(GameObject obj, float delay, float speed)
    {
        yield return new WaitForSeconds(delay);
        Image material = obj.GetComponent<Image>();
        while (material.color.a < 1)
        {
            float valDiff = 0.1f;
            if (material.color.a + 0.1f > 1)
                valDiff = material.color.a;
            material.color = new Color(material.color.r, material.color.g, material.color.b, material.color.a + (valDiff));
            yield return new WaitForSeconds(speed);
        }
    }

    IEnumerator FadeObjectOut(GameObject obj, float delay, float speed)
    {
        yield return new WaitForSeconds(delay);
        Image material = obj.GetComponent<Image>();
        while (material.color.a > 0)
        {
            float valDiff = 0.1f;
            if (material.color.a - 0.1f < 0)
                valDiff = material.color.a;
            material.color = new Color(material.color.r, material.color.g, material.color.b, material.color.a - (valDiff));
            yield return new WaitForSeconds(speed);
        }
    }

}
