﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectPanel : MonoBehaviour
{

    private float fadeTimeOut = 5;
    private float currTime;
    public bool isPanelOpen = false;
    private Dictionary<string, string> messages = new Dictionary<string, string>() {
        { "Muffin", "How about having a little snack with them?" },
        { "Bird", "Looks like something good has happened today, how about talking with them?"},
        { "Ski","Someone's down :( How about suggesting an outdoor activity somewhere to clear it all out"},
        { "Cheer","Let's cheer for the little good things!"},
        { "Cake","Someone needs a little cheering up, got any treats still stashed?"},
        {"FingerPuppet","They need comfort. Let's talk with them?"},

    };

    // Use this for initialization
    void Start()
    {
        isPanelOpen = false;
        currTime = Time.time;
        gameObject.transform.localScale = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - currTime >= fadeTimeOut)
        {
            gameObject.transform.localScale = Vector3.zero;
            isPanelOpen = false;
            currTime = Time.time;
        }
    }


    public void OpenClosePanel(string type)
    {
        if (Input.touchCount == 0)
        {
            transform.position = Input.mousePosition;
        }
        else
        {
           
            transform.position = Input.GetTouch(0).position;
        }
        type = type.Replace("(Clone)", "");
        string message = messages[type];
        if (transform.position.x > 710)
        {
            transform.position = new Vector3( 680, transform.position.y);
        }
        if (transform.position.x < 300)
        {
            transform.position = new Vector3(300, transform.position.y);
        }

        if (transform.position.y < 300)
        {
            transform.position = new Vector3(transform.position.x, 300);
        }
        if (transform.position.y> 600)
        {
            transform.position = new Vector3(transform.position.x, 600);
        }

        //if (isPanelOpen)
        //{
        //    gameObject.transform.localScale = Vector3.zero;
        //    isPanelOpen = false;
        //}
        if (!isPanelOpen)
        {
            gameObject.transform.localScale = Vector3.one;
            transform.Find("Text").GetComponent<Text>().text = message;
            isPanelOpen = true;
        }
    }

}
