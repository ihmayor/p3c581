﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NetworkIt;
using UnityEngine.UI;
using System;

/// <summary>
/// This is another template for sending events via game objects or the UI
/// Need not to use another game object for this purpose. This is to illustrate
/// how to use the system with say a UI object like a checkbox to activate some 
/// functionality
/// </summary>
public class SendPokeTemplate : MonoBehaviour {

    public NetworkItClient networkInterface;
    public bool deliverToSelf = false;
    public GameObject testFog;
    private int messageCount = 0;

    public void SendPokeMessage()
    {
        //TODO your code here
        Message m = new Message("Poke!");
        m.DeliverToSelf = deliverToSelf;
        m.AddField("num1", "" + 3);
        m.AddField("num2", "" + 4);
        m.AddField("count", "" +  messageCount++);
        networkInterface.SendMessage(m);
    }

    public void SendChirp()
    {
        Debug.Log("CHrip is Sent");
        Message m = new Message("Chirp");
        m.AddField("ID", "" + Application.platform.GetHashCode());
        networkInterface.SendMessage(m);
    }


    public void NetworkIt_Message(object m)
    {
        //TODO your code here
        Message message = (Message)m;
        Debug.Log(message.Subject);
        if (message.Subject.Contains("Enter"))
        {
            if (GetComponent<TouchControl>().enter)
                return;
            Sprite img = Resources.Load<Sprite>("bg");
            GameObject obj = GameObject.Find("Canvas").transform.Find("Panel").gameObject;
            obj.GetComponent<Image>().sprite = img;
            testFog.SetActive(true);
            GetComponent<TouchControl>().enter = true;
            AudioClip steam = Resources.Load<AudioClip>("steam");
            GetComponent<AudioSource>().PlayOneShot(steam); 
        }
        else if (message.Subject.Contains("Happy"))
        {
            Debug.Log(GameObject.FindGameObjectsWithTag("Thought").Length);
            if (GameObject.FindGameObjectsWithTag("Thought").Length <= 0)
                GetComponent<SpawnImage>().CreateHappy();
            else
            {
                GameObject obj = GameObject.Find("Canvas").transform.Find("Image").gameObject;
                obj.GetComponent<Image>().color = new Color(0.89803921568f, 0.68235294117f, 0.86666666666f);
            }
        }
        else if (message.Subject.Contains("Sad"))
        {
            Debug.Log(GameObject.FindGameObjectsWithTag("Thought").Length);
            if (GameObject.FindGameObjectsWithTag("Thought").Length <= 0)
                GetComponent<SpawnImage>().CreateCheerup();
            else
            {
                GameObject obj = GameObject.Find("Canvas").transform.Find("Image").gameObject;
                obj.GetComponent<Image>().color = new Color(0.466f, 0.6f, 0.81960784313f);
            }
        }
        else if (message.Subject.Contains("Check"))
        {
            Message tst = new Message("AddBird");
            tst.DeliverToSelf = true;
            tst.AddField("ID", "" + Application.platform.GetHashCode());
            networkInterface.SendMessage(tst);
        }
        else if (message.Subject.Contains("Leave"))
        {
            foreach (GameObject g in GameObject.FindGameObjectsWithTag("Thought"))
            {
                Destroy(g);
            }
            GetComponent<TouchControl>().enter = false;
            Sprite img = Resources.Load<Sprite>("desktop");
            GameObject obj = GameObject.Find("Canvas").transform.Find("Panel").gameObject;
            obj.GetComponent<Image>().sprite = img;
        }
    }

    public void NetworkIt_Connect(object args)
    {
        Message m = new Message("AddBird");
        m.DeliverToSelf =true;
        m.AddField("ID", "" + Application.platform.GetHashCode());
        networkInterface.SendMessage(m);
    }

    public void NetworkIt_Disconnect(object args)
    {
        //TODO your code here
        Message m = new Message("RemoveBird");
        m.DeliverToSelf = true;
        m.AddField("ID", "" + Application.platform.GetHashCode());
        networkInterface.SendMessage(m);
    }

}
