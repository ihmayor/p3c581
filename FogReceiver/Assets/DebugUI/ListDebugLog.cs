﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JW.UI;

public class ListDebugLog : UIContentManager 
{
	public enum MessageLengthEnum
	{
		Short,
		LongForErrors,
		Long
	}

	public int DisplayedMessages = 60;
	public readonly Queue<GameObject> Messages = new Queue<GameObject>();
	public MessageLengthEnum MessageLength = MessageLengthEnum.Long;
	public GameObject MessagePrefab;
    private static readonly Queue<Newtonsoft.Json.Serialization.Action> _executionQueue = new Queue<Newtonsoft.Json.Serialization.Action>();

    void Awake()
	{
		//MessagePrefab.SetActive(false);
        Application.logMessageReceivedThreaded += onLogMessage;
    }
    public void FixedUpdate()
    {
        lock (_executionQueue)
        {
            while (_executionQueue.Count > 0)
            {
                _executionQueue.Dequeue().Invoke();
            }
        }
    }
    void OnEnable()
	{
	}

	void OnDisable()
	{
        Application.logMessageReceivedThreaded -= onLogMessage;
    }

    void onLogMessage(string message, string stackTrace, LogType logType)
	{
        Debug.Log("Test");
		bool longMessage = false;
		if (MessageLength == MessageLengthEnum.Short)
			longMessage = false;
		else if (MessageLength == MessageLengthEnum.Long)
			longMessage = true;
		else if (MessageLength == MessageLengthEnum.LongForErrors)
		{
			if (logType == LogType.Warning || logType == LogType.Error || logType == LogType.Exception)
				longMessage = true;
			else
				longMessage = false;
		}

		string logString = generateLogString(message,stackTrace,logType,longMessage);
		updateTextList(logString);
	}

	string generateLogString(string message, string stackTrace, LogType logType, bool longMessage = false)
	{
		string logEntry;

		if (longMessage)
			logEntry = string.Format("{0}: {1}\n{2}"
				,logType,message,stackTrace);
		else //Short message
			logEntry = string.Format("{0}: {1}",logType, message);

		if (logType == LogType.Error || logType == LogType.Exception) //Give errors and Exceptions a red color
			logEntry = "<color=red>" + logEntry + "</color>";
		else if (logType == LogType.Warning) //Give warnings a orange color
			logEntry = "<color=orange>" + logEntry + "</color>";

		return logEntry;
	}

    IEnumerator CreateTextRow(string newLog)
    {
        Text newTextRow = CreateItemText(MessagePrefab);
        newTextRow.text = newLog;
        Messages.Enqueue(newTextRow.gameObject);

        newTextRow.gameObject.SetActive(true);
        yield return null;
    }


    /// <summary>
    /// Takes the last x messages in the list and displays them
    /// </summary>
    void updateTextList(string newLog)
	{
        Debug.Log("creating");
        //Create the new text row.
        try
        {
            lock (_executionQueue)
            {
                _executionQueue.Enqueue(() => {
                    StartCoroutine(CreateTextRow(newLog));
                });
            }
        }
        catch (System.Exception ex)
        {
            Debug.Log(ex.Message);
        }
        
        //Cap the messages at the displayed message count.
        while (Messages.Count > DisplayedMessages)
			Destroy(Messages.Dequeue());
	}
		
}
