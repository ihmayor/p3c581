﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

namespace JW.UI
{

/// <summary> Helps create/remove row/grid items on UI content views. It keeps a list of gameobject items, and uses them for UI methods.</summary>
public class UIContentManager : MonoBehaviour
{
	/// <summary> The items tracked by the Content Manager. </summary>
	public IEnumerable<GameObject> ContentItems {
		get
		{
			if (UIContentParent == null)
				return null;
			return UIContentParent.GetDirectChildren().Select(trans => trans.gameObject);
		}
	}

	public Transform UIContentParent;

	/// <summary> Creates the row in the content view using the ui prefab. </summary>
	public virtual GameObject CreateItem(GameObject prefab)
	{
		return UIInstantiate(prefab);
	}

	/// <summary> Creates the row in the content view using the UI prefab. Calls GetComponent on it with the specified type. </summary>
	public virtual T CreateItem<T>(GameObject prefab)
	{
		return this.CreateItem(prefab).GetComponent<T>();
	}
        /// <summary> Creates the row in the content view using the UI prefab. Calls GetComponent on it with the specified type. </summary>
        public virtual Text CreateItemText(GameObject prefab)
        {
            return this.CreateItem(prefab).GetComponent<Text>();
        }
        /// <summary> Deletes the row. It's also okay to just Destroy() that object, the UIContentItem handles it. </summary>
        /// <returns><c>true</c>, if row was deleted, <c>false</c> otherwise.</returns>
        public virtual bool DeleteItem(GameObject rowGo)
	{
		bool result = rowGo != null && ContentItems.ToList<GameObject>().Contains(rowGo);
		if (result)
			UnityEngine.Object.Destroy(rowGo);

		return result;
	}

	/// <summary>  </summary>
	public virtual void DeleteAllItems()
	{
		foreach (var go in ContentItems)
			UnityEngine.Object.Destroy(go);
	}

	/// <summary> Creates the UI prefab and parents it correctly. </summary>
	/// <returns>The new GameObject.</returns>
	protected GameObject UIInstantiate(GameObject prefab)
	{
		GameObject newRow = Instantiate<GameObject>(prefab);
		newRow.transform.SetParent(UIContentParent,false);
		return newRow;
	}

	/// <summary> Creates the UI prefab and parents it correctly. </summary>
	/// <returns>The new GameObject.</returns>
	protected T UIInstantiate<T>(GameObject prefab)
	{
		return UIInstantiate(prefab).GetComponent<T>();
	}
}
}