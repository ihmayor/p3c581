﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public static class ExtensionMethods
{
	public static bool IsNullOrDestroyed(UnityEngine.Object obj)
	{
		//Object.Equals is overriden by the UnityEngine, and it checks for destruction.
		return (obj == null || obj.Equals(null));
	}

	public static T FindObjectOfTypeIfNull<T>(T obj) where T : UnityEngine.Object
	{
		if (obj == null)
			obj = UnityEngine.Object.FindObjectOfType<T>();
		return obj;
	}

    #region PlaneExtensions
    public static Vector3 RayPoint(this Plane that, Ray ray)
    {
        float d;
        that.Raycast(ray, out d);
        return ray.GetPoint(d);
    }
    #endregion


	#region TransformExtenstions

	/// <summary> Returns all the children in the heriarchy under a transform. </summary>
	public static Transform[] GetAllChildren (this Transform that)
	{
		return that.GetComponentsInChildren<Transform>(true);
	}

	/// <summary> Reutrns the children under a transform, but not their children. </summary>
	public static Transform[] GetDirectChildren (this Transform that)
	{
		int childCount = that.childCount; 
		Transform[] children = new Transform[childCount];

		for (int i = 0; i < childCount; i++) 
			children[i] = that.GetChild(i);

		return children;
	}

    //Depth-first search
    public static Transform FindDeepChild(this Transform aParent, string aName)
    {
        foreach (Transform child in aParent)
        {
            if (child.name == aName)
                return child;
            var result = child.FindDeepChild(aName);
            if (result != null)
                return result;
        }
        return null;
    }

    /// <summary> Returns the topmost parent transform of a transform. </summary>
    public static Transform FindRoot (this Transform that)
	{
		Transform root = that;
		while(root.parent != null)
			root = root.parent;
		return root;
	}
	#endregion

	#region GetComponent Extensions

	public static T[] GetComponentsOnAndInChildren<T>(this Component that) where T: UnityEngine.Object
	{
		return GetComponentsOnAndInChildren<T>(that.gameObject);
	}

	public static T[] GetComponentsOnAndInChildren<T>(this GameObject that) where T : UnityEngine.Object
	{
		List<Transform> allTransforms = new List<Transform>(){that.transform};
		allTransforms.AddRange(that.transform.GetAllChildren());

		List<T> components = new List<T>();
		foreach (var trans in allTransforms)
			components.AddRange(trans.GetComponents<T>());

		return components.ToArray();
	}

	public static T GetComponentInAncestors<T>(this Component that) where T : UnityEngine.Object
	{
		return GetComponentInAncestors<T>(that.gameObject);
	}

	public static T GetComponentInAncestors<T>(this GameObject that) where T : UnityEngine.Object
	{
		Transform curr = that.transform;
		while (curr.parent != null)
		{
			curr = curr.parent;

			T component = curr.GetComponent<T>();
			if (!(component == default(T)))
				return component;
		} 
		return default(T);
	}

	public static T[] GetComponentsInAncestors<T>(this Component that) where T : UnityEngine.Object
	{
		return GetComponentsInAncestors<T>(that.gameObject);
	}

	public static T[] GetComponentsInAncestors<T>(this GameObject that) where T : UnityEngine.Object
	{
		List<T> components = new List<T>();
		Transform curr = that.transform;
		while (curr.parent != null)
		{
			curr = curr.parent;

			T component = curr.GetComponent<T>();
			if (!(component == default(T)))
				components.Add(component);
		} 
		return components.ToArray();
	}


	#endregion

	#region Debug Extensions

	/*
	/// <summary> Does a Debug log message with a pretty class name prepended to the message. </summary>
	public static void Log<T>(this Debug that, string message)
	{
		Debug.Log(string.Format("{0}: {1}",typeof(T).Name,message));
	}

	/// <summary> Does a Debug log message with a pretty class name prepended to the message. </summary>
	public static void LogWarning<T>(this Debug that, string message)
	{
		Debug.LogWarning(string.Format("{0}: {1}",typeof(T).Name,message));
	}

	/// <summary> Does a Debug log message with a pretty class name prepended to the message. </summary>
	public static void LogError<T>(this Debug that, string message)
	{
		Debug.LogError(string.Format("{0}: {1}",typeof(T).Name,message));
	}*/

	#endregion

	#region UI Extensions

	public static void Deselect(this UnityEngine.UI.Selectable that)
	{
		var eventSystems = UnityEngine.Object.FindObjectsOfType<UnityEngine.EventSystems.EventSystem>();
		foreach (var eventSystem in eventSystems)
		{
			if (eventSystem.currentSelectedGameObject == that.gameObject)
				eventSystem.SetSelectedGameObject(null); //Select the event system, a hacky way of selecting nothing.
		}
	}


	#endregion

}